import "../stylesheets/app.css";
import { default as Web3} from 'web3';
import { default as contract } from 'truffle-contract'
import EthereumBlocks from 'ethereum-blocks'
import BigNumber from 'bignumber.js'

import bitcoineum_artifacts from '../../build/contracts/Bitcoineum.json'

import BitcoineumMiner from './bitcoineum_miner';

import SimpleConsole from './simple-console';

var Bitcoineum = contract(bitcoineum_artifacts);
var accounts;
var account;

window.App = {
	start: function() {
		var self = this;


		Bitcoineum.setProvider(web3.currentProvider);

		// Get the initial account balance so it can be displayed.
		web3.eth.getAccounts(function(err, accs) {
		if (err != null) {
			alert("There was an error fetching your accounts.");
			return;
		}

		if (accs.length == 0) {
			//alert("Couldn't get any accounts! Make sure your Ethereum client is configured correctly.");
			alert("无法获取Ethereum账户信息! 请确认以太坊客户端正确启动");
			return;
		}

		accounts = accs;
		account = accounts[0];

		console.log("启动 Bitcoineum 挖矿...");
		self.setupWebConsole();

		});
  },

  setStatus: function(message) {
    var status = document.getElementById("status");
    status.innerHTML = message;
  },

  setupWebConsole: function() {
  	  var con = new SimpleConsole({
  	  	  handleCommand: handle_command,
  	  	  placeholder: "输入挖矿命令, 输入 help 获取帮助信息",
  	  	  storageID: "bitcoineum-miner"
  	  });
  	 document.body.appendChild(con.element);

     con.logHTML(
     	//"<h1>Bitcoineum Console Miner 0.3 </a></h1>"
     	"<h1>Bitcoineum 挖矿控制台 0.3 </a></h1>"
     );


	self.miner = new BitcoineumMiner(account, con.log);

    function handle_command(command){
    	// Conversational trivialities
    	var log_emoji = function(face, rotate_direction){
    		// top notch emotional mirroring
    		var span = document.createElement("span");
    		span.style.display = "inline-block";
    		span.style.transform = "rotate(" + (rotate_direction / 4) + "turn)";
    		span.style.cursor = "vertical-text";
    		span.style.fontSize = "1.3em";
    		span.innerText = face.replace(">", "〉").replace("<", "〈");
    		con.log(span);
    	};
    	
    	// Let's run through all of our supported commands
    	//
    	if(command.match(/^(stats?|statistics?)/i)) {
    		self.miner.printStats();
		} else if(command.match(/^(config|configuration)/i)) {
			self.miner.printConfig();
		} else if(command.match(/^automine/i)) {
			self.miner.autoMine();
		} else if(command.match(/^debug/i)) {
			self.miner.toggleDebug();
		} else if(command.match(/^set-mining-account/i)) {
			let selectedAccount = command.match(/(0x)?[0-9a-f]{40}/i);
			if (selectedAccount) {
				self.miner.set_mining_account(selectedAccount[0]);
			} else {
				//con.log("missing valid account");
				con.log("获取不到有效账户");
			}
		} else if(command.match(/^set-credit-account/i)) {
			let selectedAccount = command.match(/(0x)?[0-9a-f]{40}/i);
			if (selectedAccount) {
				self.miner.set_credit_account(selectedAccount[0]);
			} else {
				//con.log("missing valid account");
				con.log("获取不到有效账户");
			}
		} else if(command.match(/^set-max-spend/i)) {
			let wei = command.match(/\d+/i);
			self.miner.set_max_spend_value(wei);
		} else if(command.match(/^set-max-attempt/i)) {
			let wei = command.match(/\d+/i);
			self.miner.set_max_attempt_value(wei);
		} else if(command.match(/^set-percentage-attempt/i)) {
			let percentage = parseInt(command.match(/\d?\d?\d/i)[0]);
			self.miner.set_attempt_percentage(percentage/100);
		} else if(command.match(/^set-mine-gas/i)) {
			let quant = command.match(/\d+/i);
			self.miner.set_mine_gas(quant);
		} else if(command.match(/^set-claim-gas/i)) {
			let quant = command.match(/\d+/i);
			self.miner.set_claim_gas(quant);
		} else if(command.match(/^set-gas-price/i)) {
			let wei = command.match(/\d+/i);
			self.miner.set_gas_price(wei);
		} else if(command.match(/^check/i)) {
			let blockNumber = command.match(/\d+/i);
			self.miner.check_winner(blockNumber);
		} else if(command.match(/^claim/i)) {
			let blockNumber = command.match(/\d+/i);
			self.miner.claim_block(blockNumber);
		} else if(command.match(/^(!*\?+!*|(please |plz )?(((I )?(want|need)[sz]?|display|show( me)?|view) )?(the |some )?help|^(gimme|give me|lend me) ((the |some )?)help| a hand( here)?)/i)){
    		con.log("Bitcoineum 挖矿帮助 ");
    		con.log("stats -- 显示当前挖矿状态");
    		con.log("config -- 显示配置信息");
    		//con.log("automine -- Start/Stop mining (Requires unlocked local account");
    		con.log("automine -- 启动/停止 挖矿 (需要已解锁的本地账户");
    		//con.log("set-mining-account account -- Sets the mining account, must be an unlocked local account");
    		con.log("set-mining-account account -- 设置挖矿账户, 需要已解锁的本地账户");
    		//con.log("set-credit-account account -- Credits rewarded blocks to account, i.e a hardware wallet.");
    		con.log("set-credit-account account -- 设置获取奖励的账户, 如硬件钱包");
    		//con.log("set-max-spend wei -- Stop mining after this amount has been consumed");
    		con.log("set-max-spend wei -- 设置总消耗Wei上限（超过上限后将停止挖矿");
    		//con.log("set-max-attempt wei -- An individual mining attempt will be capped at this value");
    		con.log("set-max-attempt wei -- 设置单次最大支付Wei（每次挖矿支付的Wei将被锁定为此值");
    		//con.log("set-mine-gas quantity -- gas to use for mine");
    		con.log("set-mine-gas quantity -- 设置挖矿使用的GAS");
    		//con.log("set-claim-gas quantity -- gas to use for claim");
    		con.log("set-claim-gas quantity -- 设置获取代币的GAS");
    		//con.log("set-gas-price wei -- gas price to use for mine and claim");
    		con.log("set-gas-price wei -- 设置挖矿和获取代币的GAS价格");
    		//con.log("set-percentage-attempt % -- %100 sets the attempt to the CurrentDifficulty of the block");
    		con.log("set-percentage-attempt % -- %100 设置当前区块难度，默认为100%");
    		//con.log("check blocknumber -- Did I win a specific block");
    		//con.log("claim blocknumber -- Try to force claim a Bitcoineum Block");
    		con.log("check blocknumber -- 检查我是否赢得此区块");
    		con.log("claim blocknumber -- 尝试激活此区块奖励");
    		con.log("debug -- Enable/Disable debug events");
    		con.log("debug -- 开启/关闭 debug 模式");
    	}else{
    		var err;
    		try{
    			var result = eval(command);
    		}catch(error){
    			err = error;
    		}
    		if(err){
    			con.error(err);
    		}else{
    			con.log(result).classList.add("result");
    		}
    	}
    };
  }

}

window.addEventListener('load', function() {
  // Checking if Web3 has been injected by the browser (Mist/MetaMask)
  if (typeof web3 !== 'undefined') {
    console.warn("Using web3 detected from external source. If you find that your accounts don't appear or you have 0 MetaCoin, ensure you've configured that source properly. If using MetaMask, see the following link. Feel free to delete this warning. :) http://truffleframework.com/tutorials/truffle-and-metamask")
    // Use Mist/MetaMask's provider
    window.web3 = new Web3(web3.currentProvider);
  } else {
    console.warn("No web3 detected. Falling back to http://localhost:8545. You should remove this fallback when you deploy live, as it's inherently insecure. Consider switching to Metamask for development. More info here: http://truffleframework.com/tutorials/truffle-and-metamask");
    // fallback - use your fallback strategy (local node / hosted node + in-dapp id mgmt / fail)
    window.web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));
  }

  App.start();
});
